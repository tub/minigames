'use strict';

var SnakeFns = (function SnakeFns() {

  return {
    generateSnakeSegments: generateSnakeSegments,
    cropSnakeHistory: cropSnakeHistory
  };

  function generateSnakeSegments(snake, snakeHistory) {
    var remainingLength = snake.length;
    var snakeSegments = [];

    var points = [_.cloneDeep(snake)].concat(snakeHistory);
    for (var i = 1; i < points.length; i++) {
      var prevPoint = points[i-1];
      var point = points[i];

      if (remainingLength <= 0) { break; }

      var segmentLength = Math.abs(
          prevPoint.pos.x - point.pos.x ||
          prevPoint.pos.y - point.pos.y);

      if (points.length - 1 == i || segmentLength > remainingLength) {
        //draw until we run out of length for the last segment
        var dir = Util.invertDir(point.dir);
        snakeSegments.push({
          x1: prevPoint.pos.x, y1: prevPoint.pos.y,
          x2: prevPoint.pos.x + dir.x*remainingLength, y2: prevPoint.pos.y + dir.y*remainingLength,
          halfWidth: SnakeConst.SNAKE_HALF_WIDTH,
          type: ObstacleType.SNAKE });
        remainingLength = 0;

      } else {
        remainingLength -= segmentLength;
        snakeSegments.push({
          x1: prevPoint.pos.x, y1: prevPoint.pos.y,
          x2: point.pos.x, y2: point.pos.y,
          halfWidth: SnakeConst.SNAKE_HALF_WIDTH,
          type: ObstacleType.SNAKE });
      }
    }
    return snakeSegments;
  }

  //Reduces the snake history to be the same as its length and returns the new
  //history
  function cropSnakeHistory(snake, snakeHistory) {
    var remainingLength = snake.length;
    var points = [_.cloneDeep(snake)].concat(snakeHistory);
    var newHistory = [];
    for (var i = 1; i < points.length; i++) {
      var prevPoint = points[i-1];
      var point = points[i];

      if (remainingLength <= 0) { break; }

      var segmentLength = Math.abs(
          prevPoint.pos.x - point.pos.x ||
          prevPoint.pos.y - point.pos.y);

      if (segmentLength > remainingLength) {
        //draw until we run out of length for the last segment
        var dir = Util.invertDir(point.dir);
        var finalPos = {
          x: prevPoint.pos.x + dir.x*remainingLength,
          y: prevPoint.pos.y + dir.y*remainingLength };
        remainingLength = 0;
        newHistory.push({pos: finalPos, dir: point.dir});

      } else {
        remainingLength -= segmentLength;
        newHistory.push({pos: point.pos, dir: point.dir});
      }
    }
    return newHistory;
  }


})();
