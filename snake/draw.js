'use strict';

var Draw = (function Draw() {

  return {
    drawNonSnakeEntities: drawNonSnakeEntities,
    drawSnake: drawSnake,
    drawScore: drawScore
  };

  function drawScore(ctx, score, canvasSize) {
    ctx.textAlign = 'right';
    ctx.font = "24px arial";
    ctx.fillStyle = 'rgb(180, 180, 180)';
    ctx.fillText('' + score, canvasSize.width - 20, 30)
  }

  function drawNonSnakeEntities(ctx, nonSnakeObstacles) {
    for (var i = 0; i < nonSnakeObstacles.length; i++) {
      var obs = nonSnakeObstacles[i];
      if (obs.halfWidth > 0) {
        ctx.beginPath();
        ctx.lineWidth = obs.halfWidth * 2;
        ctx.lineCap = "butt";
        ctx.strokeStyle = 'rgb(0,0,0)';
        ctx.moveTo(obs.x1, obs.y1);
        ctx.lineTo(obs.x2, obs.y2);
        ctx.stroke();
      }
    }
  }

  function drawSnake(ctx, snake, snakeHistory) {
    var remainingLength = snake.length;
    ctx.beginPath();
    ctx.lineWidth = SnakeConst.SNAKE_WIDTH;
    ctx.lineCap = "square";
    ctx.strokeStyle = 'rgb(0,0,0)';
    ctx.moveTo(snake.pos.x + snake.dir.x, snake.pos.y + snake.dir.y);

    var points = [_.cloneDeep(snake)].concat(snakeHistory);
    for (var i = 1; i < points.length; i++) {
      var prevPoint = points[i-1];
      var point = points[i];

      if (remainingLength <= 0) { break; }

      var segmentLength = Math.abs(
          prevPoint.pos.x - point.pos.x ||
          prevPoint.pos.y - point.pos.y);

      if (segmentLength > remainingLength) {
        //draw until we run out of length for the last segment
        var dir = Util.invertDir(point.dir);
        ctx.lineTo(prevPoint.pos.x + dir.x*remainingLength, prevPoint.pos.y + dir.y*remainingLength);
        remainingLength = 0;

      } else {
        remainingLength -= segmentLength;
        ctx.lineTo(point.pos.x, point.pos.y);
      }

    }

    ctx.stroke();
  }
})();


