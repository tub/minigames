'use strict';

var EPSILON = 0.001;

var ObstacleType = {
  WALL: 0,
  SNAKE: 1,
  FOOD: 2
}

var SnakeConst = {
  SNAKE_WIDTH: 10,
  SNAKE_HALF_WIDTH: 5,
  SNAKE_EAT_GROWTH: 75
}

var FoodConst = {
  FOOD_PELLET_HALF_WIDTH: 3,
  FOOD_TIME_LIMIT: 8 * 1000 //in seconds
}
