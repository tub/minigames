'use strict';

var Levels = (function Levels() {

  var level1 = {
    snakeStart: {
      pos: { x: 200, y: 200 },
      dir: { x: 0, y: 1},
      tail: [
        { pos: { x: 200, y: 100 },
          dir: { x : 0, y: 1 } }
      ]
    },
    walls: []
  };

  var level2 = {
    snakeStart: {
      pos: { x: 200, y: 200 },
      dir: { x: 0, y: 1},
      tail: [
        { pos: { x: 200, y: 100 },
          dir: { x : 0, y: 1 } }
      ]
    },
    walls: [
        { x1: 120, y1: 80,
          x2: 120, y2: 320,
          dir: { x: 0, y: 1 },
          halfWidth: 8,
          type: ObstacleType.WALL },
        { x1: 280, y1: 80,
          x2: 280, y2: 320,
          dir: { x: 0, y: 1 },
          halfWidth: 8,
          type: ObstacleType.WALL }
      ]
  };

  var level3 = {
    snakeStart: {
      pos: { x: 200, y: 150 },
      dir: { x: 1, y: 0},
      tail: [
        { pos: { x: 100, y: 150 },
          dir: { x : 1, y: 0 } }
      ]
    },
    walls: [
        { x1: 80, y1: 100,
          x2: 320, y2: 100,
          dir: { x: 0, y: 1 },
          halfWidth: 8,
          type: ObstacleType.WALL },
        { x1: 80, y1: 300,
          x2: 320, y2: 300,
          dir: { x: 0, y: 1 },
          halfWidth: 8,
          type: ObstacleType.WALL },
        { x1: 200, y1: 160,
          x2: 200, y2: 240,
          dir: { x: 0, y: 1 },
          halfWidth: 40,
          type: ObstacleType.WALL },

      ]
  };

  var level4 = {
    snakeStart: {
      pos: { x: 50, y: 200 },
      dir: { x: 0, y: 1},
      tail: [
        { pos: { x: 50, y: 100 },
          dir: { x : 0, y: 1 } }
      ]
    },
    walls: [
        { x1: 100, y1: 60, //left
          x2: 100, y2: 240,
          dir: { x: 0, y: 1 },
          halfWidth: 8,
          type: ObstacleType.WALL },
        { x1: 100, y1: 68, //top
          x2: 260, y2: 68,
          dir: { x: 1, y: 0 },
          halfWidth: 8,
          type: ObstacleType.WALL },

        { x1: 300, y1: 140, //right
          x2: 300, y2: 320,
          dir: { x: 0, y: 1 },
          halfWidth: 8,
          type: ObstacleType.WALL },
        { x1: 140, y1: 312, //bottom
          x2: 300, y2: 312,
          dir: { x: 1, y: 0 },
          halfWidth: 8,
          type: ObstacleType.WALL }
      ]
  };

  var level5 = {
    snakeStart: {
      pos: { x: 20, y: 200 },
      dir: { x: 0, y: 1},
      tail: [
        { pos: { x: 20, y: 100 },
          dir: { x : 0, y: 1 } }
      ]
    },
    walls: [
        { x1: 200, y1: 155, //centre
          x2: 200, y2: 245,
          dir: { x: 0, y: 1 },
          halfWidth: 45,
          type: ObstacleType.WALL },
        { x1: 80, y1: 35, //top left
          x2: 80, y2: 125,
          dir: { x: 0, y: 1 },
          halfWidth: 45,
          type: ObstacleType.WALL },
        { x1: 320, y1: 35, //top right
          x2: 320, y2: 125,
          dir: { x: 0, y: 1 },
          halfWidth: 45,
          type: ObstacleType.WALL },
        { x1: 80, y1: 275, //bottom left
          x2: 80, y2: 365,
          dir: { x: 0, y: 1 },
          halfWidth: 45,
          type: ObstacleType.WALL },
        { x1: 320, y1: 275, //bottom right
          x2: 320, y2: 365,
          dir: { x: 0, y: 1 },
          halfWidth: 45,
          type: ObstacleType.WALL },
      ]
  };

  function generateBoundryWalls(canvasSize) {
    return [
      { x1: 0, y1: 0,
        x2: canvasSize.width, y2: 0, halfWidth: 0,
        dir: { x: 1, y: 0 },
        type: ObstacleType.WALL }, //top
      { x1: canvasSize.width, y1: 0, x2: canvasSize.width,
        y2: canvasSize.height, halfWidth: 0,
        dir: { x: 0, y: 1 },
        type: ObstacleType.WALL  }, //right
      { x1: canvasSize.width, y1: canvasSize.height,
        x2: 0, y2: canvasSize.height, halfWidth: 0,
        dir: { x: -1, y: 0 },
        type: ObstacleType.WALL }, //bottom
      { x1: 0, y1: canvasSize.height,
        x2: 0, y2: 0, halfWidth: 0,
        dir: { x: 0, y: -1 },
        type: ObstacleType.WALL  } //left
    ];
  }


  return {
    generateBoundryWalls: generateBoundryWalls,
    levels: [
      level1,
      level2,
      level3,
      level4,
      level5]
  };


})();

