'use strict';

var FoodFns = (function FoodFns() {

  return {
    generateRandomFoodPellet: generateRandomFoodPellet,
    removePelletsAndUpdateState: removePelletsAndUpdateState,
    updateFoodState: updateFoodState
  };

  //Creates a food pellet with a random, but valid position, in the map
  function generateRandomFoodPellet(canvasSize, snakeSegments, nonSnakeObstacles) {
    //try random positions until we find one that's valid
    while(true) {
      var randX = Math.floor(Math.random() * canvasSize.height);
      var randY = Math.floor(Math.random() * canvasSize.height);
      var randPt = new toxi.geom.Vec2D({x: randX, y: randY});

      var obstacles = nonSnakeObstacles.concat(snakeSegments);

      //position is valid if it's further away from any other obstacle by both
      //of their widths
      var valid = true;
      for(var i = 0; i < obstacles.length; i++) {
        var obs = obstacles[i];
        var obsSegment = new toxi.geom.Line2D(
          new toxi.geom.Vec2D({x: obs.x1, y: obs.y1}),
          new toxi.geom.Vec2D({x: obs.x2, y: obs.y2})
        );

        var closestPointOnObs = obsSegment.closestPointTo(randPt);
        var distSq = closestPointOnObs.distanceToSquared(randPt);
        if (distSq <=
            FoodConst.FOOD_PELLET_HALF_WIDTH * FoodConst.FOOD_PELLET_HALF_WIDTH +
            obs.halfWidth * obs.halfWidth) {
          valid = false;
          break;
        }
      }

      if (valid) {
        return createFoodPellet(randX, randY);
      }

    }
  }

  function createFoodPellet(x, y) {
    return {
      x1: x, y1: y - FoodConst.FOOD_PELLET_HALF_WIDTH,
      x2: x, y2: y + FoodConst.FOOD_PELLET_HALF_WIDTH,
      dir: { x: 0, y: 1 },
      halfWidth: FoodConst.FOOD_PELLET_HALF_WIDTH,
      type: ObstacleType.FOOD,
      createTime: Date.now()};
  }

  function findExpiredPellets(pellets, nowDate) {
    var expired = [];
    var now = nowDate.getTime();
    for (var i = 0; i < pellets.length; i++) {
      if (pellets[i].createTime + FoodConst.FOOD_TIME_LIMIT < now) {
        expired.push(pellets[i]);
      }
    }
    return expired;
  }

  function getNextAddFoodTime(pellets, lastRemovedTime) {
    if (pellets.length > 1 || !lastRemovedTime) { return null; }

    //we want to create new food sometime within 5 seconds of the last removed Time
    var fiveSecondsInMs = 5 * 1000;
    var last = lastRemovedTime.getTime();
    var randMs = Math.floor(Math.random() * fiveSecondsInMs);
    var addtime = lastRemovedTime.getTime() + Math.floor(Math.random() * fiveSecondsInMs);
    return new Date(addtime);
  }

  function isSameFoodPellet(a, b) {
    return a.x1 == b.x1 && a.y1 == b.y1;
  }

  function removePelletsAndUpdateState(foodState, toRemove, nowDate) {
    if (!toRemove || !toRemove.length) { return foodState; }

    var newPellets = [];
    for(var i = 0; i < foodState.pellets.length; i++) {
      var orig = foodState.pellets[i];
      if (!_.some(toRemove, function(x) { return isSameFoodPellet(orig, x); })) {
        newPellets.push(orig);
      }
    }

    if (foodState.pellets.length != newPellets.length) {
      return {
        pellets: newPellets,
        lastRemoveTime: nowDate,
        nextAddFoodTime: getNextAddFoodTime(newPellets, nowDate)
      };
    } else {
      //we haven't removed anything?
      console.log('Didn\'t remove anything on removePelletsAndUpdateState');
      return foodState; 
    }
  }

  function updateFoodState(foodState, nowDate, canvasSize, snakeSegments, nonSnakeObstacles) {
    var expired = findExpiredPellets(foodState.pellets, nowDate);
    var newState = _.cloneDeep(foodState);
    newState = removePelletsAndUpdateState(newState, expired, nowDate);

    if (newState.nextAddFoodTime && newState.nextAddFoodTime.getTime() <= nowDate.getTime()) {
      //by passing in the old nonSnakeObstacles we won't generate at the same position as
      //old food pellets, which is probably a good thing
      var foodPellet = generateRandomFoodPellet(canvasSize, snakeSegments, nonSnakeObstacles);
      newState.pellets.push(foodPellet);
      newState.nextAddFoodTime = getNextAddFoodTime(newState.pellets, nowDate);
    }

    return newState;
  }

})();
