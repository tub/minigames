'use strict';

var Collision = (function Collision() {

  return {
    getCollision: getCollision
  };

  //TODO: there's a potential bug here where a snake that's going fast enough
  //can *jump* over a parallel collision
  function getCollision(snake, snakeSegments, nonSnakeObstacles) {

    //iterate through all of the obstacles and see if there's a collision with the snake head
    //each obstacle is a vector with a width
    var headStart = new toxi.geom.Vec2D({
          x: snake.pos.x + snake.dir.x * SnakeConst.SNAKE_HALF_WIDTH,
          y: snake.pos.y + snake.dir.y * SnakeConst.SNAKE_HALF_WIDTH});
    var headEnd = new toxi.geom.Vec2D({x: snakeSegments[0].x2, y: snakeSegments[0].y2});

    snakeSegments = snakeSegments.slice();
    //remove the head - thats what we're testing for
    snakeSegments.shift();
    //remove the following segment - it will
    //a) always intersect with the head where they connect
    //b) never intersect otherwise
    if (snakeSegments.length) { snakeSegments.shift(); }

    var obstacles = nonSnakeObstacles.concat(snakeSegments);

    for (var i = 0; i < obstacles.length; i++) {
      var obs = obstacles[i];
      var headSegment = new toxi.geom.Line2D(
        //extend the start of the head segment by the width of the snake and the obs we're checking
        //Note: actually the width of the obs only affects intersections, not parrallel collisions,
        //we account for that later by drawing the normal form the real head
        new toxi.geom.Vec2D({
          x: snake.pos.x +
            snake.dir.x * (SnakeConst.SNAKE_HALF_WIDTH + obs.halfWidth),
          y: snake.pos.y +
            snake.dir.y * (SnakeConst.SNAKE_HALF_WIDTH + obs.halfWidth)}),
        headEnd
      );

      var obsSegment;
      if (obs.type === ObstacleType.WALL || obs.type === ObstacleType.FOOD) {
        //walls have to butt out if the snake is to hit them at it's centre at the edge
        //of the wall when intersecting.
        obsSegment = new toxi.geom.Line2D(
          new toxi.geom.Vec2D({
            x: obs.x1 - obs.dir.x * SnakeConst.SNAKE_HALF_WIDTH,
            y: obs.y1 - obs.dir.y * SnakeConst.SNAKE_HALF_WIDTH}),
          new toxi.geom.Vec2D({
            x: obs.x2 + obs.dir.x * SnakeConst.SNAKE_HALF_WIDTH,
            y: obs.y2 + obs.dir.y * SnakeConst.SNAKE_HALF_WIDTH})
        );
      } else {
        obsSegment = new toxi.geom.Line2D(
          new toxi.geom.Vec2D({x: obs.x1, y: obs.y1}),
          new toxi.geom.Vec2D({x: obs.x2, y: obs.y2})
        );
      }
      var intersection = headSegment.intersectLine(obsSegment);

      //TODO - Bug intersecting with third segment when doing a quick left-right or right-left

      //if the obstacle is coincident then we must have collided
      if (intersection.type === toxi.geom.Line2D.LineIntersection.Type.COINCIDENT) {
        return obs;

      } else if (intersection.type == null || intersection.type === toxi.geom.Line2D.LineIntersection.Type.PARALLEL) {
        //extend the snake segments to account for the end caps
        var extSegment;
        if (obs.type === ObstacleType.SNAKE) {
          var segDir = obsSegment.getDirection();
          extSegment = new toxi.geom.Line2D(
            new toxi.geom.Vec2D({x: obs.x1 - segDir.x*obs.halfWidth, y: obs.y1 - segDir.y*obs.halfWidth}),
            new toxi.geom.Vec2D({x: obs.x2 + segDir.x*obs.halfWidth, y: obs.y2 + segDir.y*obs.halfWidth})
          );
        } else {
          extSegment = new toxi.geom.Line2D(
            new toxi.geom.Vec2D({x: obs.x1, y: obs.y1}),
            new toxi.geom.Vec2D({x: obs.x2, y: obs.y2}));
        }

        //if the obstacle is running parallel then iff it's at the same
        //orthagonal position as the head, we have collided - if the width is
        //greater than the distance between them
        //Note: we use head start, not the start of the headSegment. The headSegment is actually
        //pushed forward by the obs halfWidth which we don't want for the parallel case
        var closestPointOnSegmentToHead = extSegment.closestPointTo(headStart);
        var lineToSegmentFromHead = new toxi.geom.Line2D(headStart, closestPointOnSegmentToHead);
        //if the closest point from the head to the segment isn't on a cardinal
        //direction then there can't be a collision
        var isNorm = Math.abs(lineToSegmentFromHead.a.x - lineToSegmentFromHead.b.x) - EPSILON <= 0
          || Math.abs(lineToSegmentFromHead.a.y - lineToSegmentFromHead.b.y) - EPSILON <= 0;
        if (!isNorm) { continue; }

        //Note: toxi.geom.Line2D seems to have a bug where the result can be null!
        //That seems to mean its coincident, but not intersecting/touching.
        //We have to caluclate the distance as a special case. Some funky
        //caps/no caps things going on again :(
        if (intersection.type == null) {
          //check the distance
          var lenSq = lineToSegmentFromHead.getLengthSquared();
          if (lenSq === 0) {
            return obs;
          }
        } else {
          //check the distance
          var lenSq = lineToSegmentFromHead.getLengthSquared();
          var width = obs.halfWidth + SnakeConst.SNAKE_HALF_WIDTH;
          if (lenSq < width * width) {
            return obs;
          }
        }


      } else if (intersection.type === toxi.geom.Line2D.LineIntersection.Type.INTERSECTING) {
        return obs;
      } else if (intersection.type === toxi.geom.Line2D.LineIntersection.Type.NON_INTERSECTING) {
        continue;
      }
    }
    return null;
  }

})();
