
var Util = (function Util() {

  return {
    invertDir: invertDir
  };

  function invertDir(dir) {
    return { x: -dir.x, y: -dir.y };
  }

})();
