'use strict';

var Game = (function() {

  var gameOverDiv;
  var ctx;

  var gameOver = false;
  var canvasSize;

  var score = 0;
  var currentLevel = 0;

  var levelTargetScore = 80;

  var snake;
  var snakeHistory;
  var boundryWalls;
  var level;

  var foodState;

  function init(ctxparam, document, canvasSizeParam, gameOverDivParam){
    canvasSize = canvasSizeParam;
    ctx = ctxparam;
    gameOverDiv = gameOverDivParam;

    boundryWalls = Levels.generateBoundryWalls(canvasSize);

    levelInit(currentLevel);

    //start the game loop
    window.requestAnimationFrame(mainloop);
    document.onkeydown = onKeyDown;
  }

  function levelInit(levelNum) {
    level = Levels.levels[levelNum];

    snake = {
      pos: _.clone(level.snakeStart.pos),
      dir: _.clone(level.snakeStart.dir), 
      length: 100,
      speed: 2,
    };

    //setup the history (which forms the snake segments) with tail segments
    //and the current point
    snakeHistory = [];
    _.forEach(level.snakeStart.tail, function (tailPiece) {
      snakeHistory.push(_.merge({}, snake, tailPiece));
    });
    snakeHistory.push(_.cloneDeep(snake));

    setInitialFoodState();
  }

  function setInitialFoodState() {
    foodState = {
      pellets: [],
      nextAddFoodTime: null,
      lastRemovePelletTime: null
    };

    var snakeSegments = SnakeFns.generateSnakeSegments(snake, snakeHistory);
    var nonSnakeObstacles = getAllNonSnakeObstacles();

    //add some food to start with
    var foodPellet = FoodFns.generateRandomFoodPellet(canvasSize, snakeSegments, nonSnakeObstacles);
    foodState.pellets.push(foodPellet);
  }


  function onKeyDown(e) {
    e = e || window.event;

    if (e.keyCode == '37') { // left arrow
      turnLeft();
    } else if (e.keyCode == '39') { // right arrow
      turnRight();
    }
    
    //if the user presses twice really quicky will that break anything?
  }

  function turnLeft() {
    snake = _.merge({}, snake, {dir: {x: snake.dir.y, y: -snake.dir.x}});
    snakeHistory.unshift(_.cloneDeep(snake));
  }

  function turnRight() {
    snake = _.merge({}, snake, {dir: {x: -snake.dir.y, y: snake.dir.x}});
    snakeHistory.unshift(_.cloneDeep(snake));
  }

  function mainloop() {
    if (gameOver) { return; }

    var snakeSegments = SnakeFns.generateSnakeSegments(snake, snakeHistory);
    var nonSnakeObstacles = getAllNonSnakeObstacles();

    //handle user input (or is this done in the events)
    var collision = Collision.getCollision(snake, snakeSegments, nonSnakeObstacles);
    if (collision != null && collision.type === ObstacleType.FOOD) {
      //TODO: but first, clip the snake history to it's current history
      snakeHistory = SnakeFns.cropSnakeHistory(snake, snakeHistory);
      //the smake ate food. So make it grow
      snake = _.merge({}, snake, {length: snake.length + SnakeConst.SNAKE_EAT_GROWTH});
      //remove the food from the game board
      foodState = FoodFns.removePelletsAndUpdateState(foodState, [collision], new Date());
      //update the score
      score += 10;

      if (score > 0 && score % levelTargetScore == 0) {
        //switch to the next level
        currentLevel += 1;
        levelInit(currentLevel % Levels.levels.length);
      }

    } else if (collision != null) {
      gameOver = true;
      gameOverDiv.style.display = '';
    }

    //update game state (move snake)
    snake = _.merge({}, { pos: {
      x: snake.pos.x += snake.dir.x * snake.speed,
      y: snake.pos.y += snake.dir.y * snake.speed
    }}, snake);

    //clear canvas
    ctx.clearRect(0,0,400,400); // clear canvas
    
    Draw.drawNonSnakeEntities(ctx, nonSnakeObstacles);
    Draw.drawSnake(ctx, snake, snakeHistory);
    Draw.drawScore(ctx, score, canvasSize);

    //we probably don't need to do this for every tick
    foodState = FoodFns.updateFoodState(foodState, new Date(), canvasSize, snakeSegments, nonSnakeObstacles);

    window.requestAnimationFrame(mainloop);
  }

  function getAllNonSnakeObstacles() {
    var obstacles = [];
    obstacles = obstacles.concat(boundryWalls);
    obstacles = obstacles.concat(level.walls);
    obstacles = obstacles.concat(foodState.pellets);
    return obstacles;
  }

  return { 
    init: init
  };

})();

