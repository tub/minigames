'use strict';

var Game = (function() {

  var gameOverDiv;
  var canvas;
  var ctx;
  var prevUpdateTime;

  var gameOver = false;
  var canvasSize;
  var state = Immutable.Map({
    gameOver: false,
    towers: Immutable.Map(),
    enemies: Immutable.Map(),
    explosions: Immutable.List(),
    draggingTowerId: null,
    towerInFiringArcId: null,
  });;

  var score = 0;

  const Explosion = Immutable.Record({
    x: 0,
    y: 0,
    expiresAt: null,
    type: null //TODO: not currently used
    //we should be able to determine the state of the explosion from
    //the type, and expiresAt.
    //We may need to add some more if there was randomisation involved within
    //the type. A seed or hash or something
  });

  function init(ctxparam, canvasParam, document, canvasSizeParam, gameOverDivParam){
    canvas = canvasParam;
    canvasSize = canvasSizeParam;
    ctx = ctxparam;
    gameOverDiv = gameOverDivParam;

    const upFacing = (3*Math.PI)/2;
    const twentyDegrees = (Math.PI/9); //twenty degrees
    const rotationSpeed = (Math.PI/6); //thirty degrees

    const Tower = Immutable.Record({
      id: 0,
      x: 200,
      y: 200,
      facing: upFacing,
      intendedFacing: upFacing,
      halfSpread: twentyDegrees,
      platformRadius: 7,
      maxRange: 120,
      minRange: 30,
      maxRangeSquared: 120*120,
      minRangeSquared: 30*30,
      rotationSpeed: rotationSpeed, //degrees per second
      fireRate: 1250, //duration between shots in ms
      canFireNextAt: undefined,
    });

    const tower1 = new Tower({id: '1', x: 150});
    state = state.updateIn(['towers'], v => v.set(tower1.id, tower1));
    const tower2 = new Tower({id: '2', x: 250});
    state = state.updateIn(['towers'], v => v.set(tower2.id, tower2));

    const Enemy = Immutable.Record({
      id: 0,
      x: 200,
      y: 0,
      velocity: 2.0 / 60
    });

    const enemy1 = new Enemy({id: '1', x: 135});
    state = state.updateIn(['enemies'], v => v.set(enemy1.id, enemy1));
    const enemy2 = new Enemy({id: '2', x: 300});
    state = state.updateIn(['enemies'], v => v.set(enemy2.id, enemy2));

    prevUpdateTime = performance.now(); //start the previous time at some point

    //start the game loop
    window.requestAnimationFrame(mainloop);
    canvas.onmousemove = onmousemove;
    canvas.onmousedown = onmousedown;
    canvas.onmouseup = onmouseup;
  }

  function mainloop() {
    if (gameOver) { return; }

    const updateTime = performance.now();
    const timeDiff = updateTime - prevUpdateTime;
    prevUpdateTime = updateTime;

    stateUpdate(timeDiff, updateTime);

    ctx.clearRect(0,0,400,400); // clear canvas
    draw();

    window.requestAnimationFrame(mainloop);
  }

  function stateUpdate(timeDiff, updateTime) {
    //update existing explosions
    const explosions = state.get('explosions');
    const updatedExplosions = updateExplosions(explosions, updateTime);

    const towers = rotateTowers(timeDiff, state.get('towers'));
    let enemies = state.get('enemies');
    enemies = moveEnemies(timeDiff, state.get('enemies'));
    state = state.set('enemies', enemies);

    //shooting
    //fire new shots
    const [updatedTowers, newExplosions] =
      performShooting(towers, enemies, updateTime);

    state = state.set('towers', updatedTowers);
    state = state.set('explosions', updatedExplosions.concat(newExplosions));
  }

  function performShooting(towers, enemies, updateTime) {
    var explosions = [];
    const updatedTowers = towers.map(t => {
      if (!t.canFireNextAt || t.canFireNextAt <= updateTime) {
        //TODO: tower should only be able to fire at one enemy at a time.
        //it should fire on the closest (well ideally it would fire where
        //it would deal the most damage... that could get pretty complicated)
        var anyHits = false;
        enemies.forEach(e => {
          const [hit, distsq] = isHitAndDistanceSq(t, e);
          if (hit) {
            explosions.push(new Explosion({
              x: e.x,
              y: e.y,
              expiresAt: updateTime + 500
            }));
            anyHits = true;
          }
        });
        if (anyHits) {
          return t.set('canFireNextAt', updateTime + t.fireRate);
        } else {
          return t;
        }
      } else {
        return t;
      }
    });

    return [updatedTowers, explosions];
  }

  function updateExplosions(explosions, updateTime) {
    return explosions.filter(exp => exp.expiresAt > updateTime);
  }

  function drawExplosions(explosions, updateTime) {
    explosions.forEach(exp => {
      ctx.fillStyle = "rgba(255, 102, 0, 0.8)"; //blase orange (255, 102, 0)
      ctx.beginPath();
      ctx.arc(exp.x, exp.y, 20, 0, Math.PI*2, false); // explosion circle
      ctx.fill();
    });
  }

  function moveEnemies(timeDiff, enemies) {
    const updatedEnemies = enemies.map(e => {
      const y = e.y + timeDiff * e.velocity;
      return e.set('y', y);
    });
    return updatedEnemies.filter(e => {
      return (e.y - 50 < canvasSize.height);
    });
  }

  function rotateTowers(timeDiff, towers) {
    const timeDiffInSec =  timeDiff / 1000.0;
    const rotatedTowers = towers.map(t => {
      if (t.facing != t.intendedFacing) {
        //update facing to be closer to intended facing
        const rotationAmount = timeDiffInSec * t.rotationSpeed;
        //which way should we rotate?
        let facingDiff = t.intendedFacing - t.facing;
        let rotationDir = 1;
        while (facingDiff < 0) { facingDiff += Math.PI * 2 }
        if (facingDiff > Math.PI) { //if we have to rotate farther than 180 to get to the facing diff, go the other way
          rotationDir = -1;
        }
        let newFacing = t.facing + rotationAmount * rotationDir;
        //If we would over-rotate, stop at the intended facing
        if (t.facing < t.intendedFacing && t.intendedFacing < newFacing) {
          newFacing = t.intendedFacing;
        } else if (t.intendedFacing < t.facing && newFacing < t.intendedFacing ) {
          newFacing = t.intendedFacing;
        }
        while (newFacing < 0) {
          newFacing += Math.PI * 2
        }
        newFacing = newFacing % (Math.PI * 2)
        t = t.set('facing', newFacing);
      }
      return t;
    });
    return rotatedTowers;
  }

  function draw() {
    if (!state) { return; }

    const towers = state.get('towers');
    if (towers) {
      drawTowers(towers);
    }

    const enemies = state.get('enemies');
    if (enemies) {
      drawEnemies(enemies);
    }

    const explosions = state.get('explosions');
    if (explosions) {
      drawExplosions(explosions);
    }
  }

  function drawEnemies(enemies) {
    enemies.forEach(e => {
      drawChevron(e);
    });
  }

  function drawTowers(towers) {
    towers.forEach(t => {
      ctx.fillStyle = "rgba(0, 0, 200, 1)";
      ctx.beginPath();
      ctx.arc(t.x, t.y, t.platformRadius, 0, Math.PI*2, true); // tower circle
      ctx.fill();

      if (t.id == state.get('towerInFiringArcId')) {
        ctx.fillStyle = "rgba(0, 0, 200, 0.20)"; //highlight hovering tower
        ctx.strokeStyle = "rgba(0, 0, 250, 1)";
      } else {
        ctx.fillStyle = "rgba(0, 0, 200, 0.1)";
        ctx.strokeStyle = "rgba(0, 0, 200, 1)";
      }

      ctx.beginPath();
      ctx.arc(t.x, t.y, t.maxRange, t.facing - t.halfSpread, t.facing + t.halfSpread, false); // outer arc
      ctx.arc(t.x, t.y, t.minRange, t.facing + t.halfSpread, t.facing - t.halfSpread, true); // inner arc
      ctx.closePath();
      ctx.stroke();
      ctx.fill();
    });
  }

  function onmousemove(e) {
    const pos = {x: (e.x - canvas.offsetLeft), y: (e.y - canvas.offsetTop)};
    const draggingTowerId = state.get('draggingTowerId');
    if (draggingTowerId) {
      const origDraggingTower = state.getIn(['towers', draggingTowerId]);
      const t = updateMainAngle(pos, origDraggingTower);
      state = state.updateIn(['towers'], v => v.set(draggingTowerId, t));
    } else {
      const closestHitTowerInfo = state.get('towers')
        .map(t => [t, ...isHitAndDistanceSq(t, pos)])
        .filter(([t, hit, distancesq]) => hit)
        .sortBy(([t, hit, distancesq]) => distancesq)
        .first();
      if (closestHitTowerInfo) {
        const [t, hit, distancesq] = closestHitTowerInfo;
        state = state.set('towerInFiringArcId', t.id);
      } else {
        state = state.set('towerInFiringArcId', null);
      }
    }
  }

  function updateMainAngle(pos, t) {
    const towerCenter = new toxi.geom.Vec2D(t);
    let theta = new toxi.geom.Vec2D(pos).sub(towerCenter).heading();
    while (theta < 0) {
      theta += Math.PI * 2
    }
    return t.set('intendedFacing', theta);
  }

  function isHitAndDistanceSq(t, pos) {
    const point = new toxi.geom.Vec2D(pos);
    const towerCenter = new toxi.geom.Vec2D(t);

    //we scale by a factor to ensure triangle covers the complete arc...
    //this is a bit rubish, and won't work if the arc is wide enough (think 180 degrees)
    //we should change this to check if the angle to the point is inbetween these angles
    const trianglePoint2 = toxi.geom.Vec2D.fromTheta(t.facing - t.halfSpread).scale(t.maxRange * 1.4).add(towerCenter);
    const trianglePoint3 = toxi.geom.Vec2D.fromTheta(t.facing + t.halfSpread).scale(t.maxRange * 1.4).add(towerCenter);

    const triangle = new toxi.geom.Triangle2D(towerCenter);
    if (point.isInCircle(towerCenter, t.maxRange) &&
      !point.isInCircle(towerCenter, t.minRange) &&
      point.isInTriangle(towerCenter, trianglePoint2, trianglePoint3)) {
      return [true, towerCenter.distanceToSquared(point)];
    } else {
      return [false, -1];
    }
  }

  function onmousedown(e) {
    state = state.set('draggingTowerId', state.get('towerInFiringArcId'));
  }

  function onmouseup() {
    state = state.set('draggingTowerId', null);
  }

  function drawChevron(enemy)
  {
    //draw chevron
    const wingAngle = -Math.PI / 5.0; //36 degrees
    const edgeAngle = -Math.PI / 2.0 + wingAngle; //126 degrees (90 + wingAngle)
    const chevronWingSpan = 18.0;
    const chevronWingWidth = 4.0;
    const chevronExtraWidth = 1.0;

    let leadingPoint = new toxi.geom.Vec2D(enemy.x, enemy.y);

    let leadingWingEdgeLeft = new toxi.geom.Vec2D(-chevronWingSpan, 0);
    leadingWingEdgeLeft = leadingWingEdgeLeft.getRotated(-wingAngle);
    leadingWingEdgeLeft = leadingWingEdgeLeft.add(leadingPoint);

    let trailingWingEdgeLeft = new toxi.geom.Vec2D(-chevronWingWidth, 0);
    trailingWingEdgeLeft = trailingWingEdgeLeft.getRotated(-edgeAngle);
    trailingWingEdgeLeft = trailingWingEdgeLeft.add(leadingWingEdgeLeft);

    let leadingWingEdgeRight = new toxi.geom.Vec2D(chevronWingSpan, 0);
    leadingWingEdgeRight = leadingWingEdgeRight.getRotated(wingAngle);
    leadingWingEdgeRight = leadingWingEdgeRight.add(leadingPoint);

    let trailingWingEdgeRight = new toxi.geom.Vec2D(chevronWingWidth, 0);
    trailingWingEdgeRight = trailingWingEdgeRight.getRotated(edgeAngle);
    trailingWingEdgeRight = trailingWingEdgeRight.add(leadingWingEdgeRight);

    let trailingPointExtd = new toxi.geom.Vec2D(chevronWingSpan, 0);
    trailingPointExtd = trailingPointExtd.getRotated(-Math.PI + wingAngle);
    trailingPointExtd = trailingPointExtd.add(trailingWingEdgeRight);

    let trailingEdge = new toxi.geom.Line2D(trailingWingEdgeRight, trailingPointExtd);
    let vetLine = new toxi.geom.Line2D(leadingPoint, leadingPoint.add(0, -2*chevronWingWidth));

    const intersect = trailingEdge.intersectLine(vetLine).getPos();

    //back flaps
    let backWingEdgeRight = new toxi.geom.Vec2D(chevronWingWidth + chevronExtraWidth, 0);
    backWingEdgeRight = backWingEdgeRight.getRotated(edgeAngle);
    backWingEdgeRight = backWingEdgeRight.add(leadingWingEdgeRight);

    let backPointExtd = new toxi.geom.Vec2D(chevronWingSpan, 0);
    backPointExtd = backPointExtd.getRotated(-Math.PI + wingAngle);
    backPointExtd = backPointExtd.add(backWingEdgeRight);

    let backEdge = new toxi.geom.Line2D(backWingEdgeRight, backPointExtd);

    const intersectBack = backEdge.intersectLine(vetLine).getPos();

    let backWingEdgeLeft = new toxi.geom.Vec2D(-chevronWingWidth + -chevronExtraWidth, 0);
    backWingEdgeLeft = backWingEdgeLeft.getRotated(-edgeAngle);
    backWingEdgeLeft = backWingEdgeLeft.add(leadingWingEdgeLeft);

    ctx.strokeStyle = "rgba(250, 50, 50, 1)";
    ctx.fillStyle = "rgba(250, 50, 50, 1)";
    ctx.beginPath();
    ctx.moveTo(intersect.x, intersect.y);
    ctx.lineTo(trailingWingEdgeRight.x, trailingWingEdgeRight.y);
    ctx.lineTo(backWingEdgeRight.x, backWingEdgeRight.y);
    ctx.lineTo(intersectBack.x, intersectBack.y);
    ctx.lineTo(backWingEdgeLeft.x, backWingEdgeLeft.y);
    ctx.lineTo(trailingWingEdgeLeft.x, trailingWingEdgeLeft.y);
    ctx.closePath();
    ctx.stroke();
    ctx.fill();

    ctx.strokeStyle = "rgba(255, 110, 110, 1)";
    ctx.fillStyle = "rgba(255, 110, 110, 1)";
    ctx.beginPath();
    ctx.moveTo(leadingPoint.x, leadingPoint.y);
    ctx.lineTo(leadingWingEdgeRight.x, leadingWingEdgeRight.y);
    ctx.lineTo(trailingWingEdgeRight.x, trailingWingEdgeRight.y);
    ctx.lineTo(intersect.x, intersect.y);
    ctx.closePath();
    ctx.stroke();
    ctx.fill();

    ctx.strokeStyle = "rgba(255, 150, 150, 1)";
    ctx.fillStyle = "rgba(255, 150, 150, 1)";
    ctx.beginPath();
    ctx.moveTo(leadingPoint.x, leadingPoint.y);
    ctx.lineTo(leadingWingEdgeLeft.x, leadingWingEdgeLeft.y);
    ctx.lineTo(trailingWingEdgeLeft.x, trailingWingEdgeLeft.y);
    ctx.lineTo(intersect.x, intersect.y);
    ctx.closePath();
    ctx.stroke();
    ctx.fill();
  }


  return { 
    init: init
  };

})();
