'use strict';

const App = (function() {

  let base;

  const regions = [{
    name: 'A',
    verticies: [
      { x: 130, y: 80 },
      { x: 160, y: 110 }, // shared with B
      { x: 150, y: 150 }, // shared with B
      { x: 135, y: 160 }, // shared with B
      { x: 145, y: 180 }, // part shared with B
      { x: 110, y: 210 }, // shared with C
      { x:  90, y: 190 },
      { x: 100, y: 165 },
      { x: 100, y: 105 },
    ],
  }, {
    name: 'B',
    verticies: [
      { x: 190, y: 100 },
      { x: 220, y: 130 },
      { x: 225, y: 165 },
      { x: 195, y: 162 }, // shared with D
      { x: 195, y: 210 }, // shared with D
      { x: 159, y: 210 }, // shared with D
      { x: 145, y: 180 }, // shared with C
      { x: 135, y: 160 }, // shared with A
      { x: 150, y: 150 },
      { x: 160, y: 110 },
    ],
  }, {
    name: 'C',
    verticies: [
      { x: 145, y: 180 }, // shared with B
      { x: 159, y: 210 }, // shared with D *3
      { x: 160, y: 230 }, // shared with D *2
      { x: 195, y: 235 }, // *1
      { x: 192, y: 270 },
      { x: 150, y: 270 },
      { x: 130, y: 255 },
      { x: 100, y: 255 },
      { x:  90, y: 240 },
      { x: 110, y: 210 }, // shared with A
    ],
  }, {
    name: 'D',
    verticies: [
      { x: 195, y: 162 }, // shared with B
      { x: 225, y: 165 }, // shared with B
      { x: 270, y: 175 },
      { x: 275, y: 220 },
      { x: 260, y: 255 },
      { x: 220, y: 255 },
      { x: 195, y: 235 }, // shared with C *1
      { x: 160, y: 230 }, // shared with C *2
      { x: 159, y: 210 }, // shared with B *3
      { x: 195, y: 210 }, // shared with B
    ],
  }]

  let hoverRegionName = null

  function onMouseMove(e) {
    //TODO: extract this logic
    if (document.elementFromPoint(e.clientX, e.clientY).id === 'canvas') {
      const mousePosition = { x: e.offsetX, y: e.offsetY }
      const hoverRegion = findRegionContainingPoint(mousePosition)
      hoverRegionName = hoverRegion && hoverRegion.name
    } else {
      hoverRegionName = null
    }
  }

  function findRegionContainingPoint(x, y) {
    const pt = new toxi.geom.Vec2D(x, y)
    for (let region of regions) {
      const polyPoints = region.verticies.map(x => new toxi.geom.Vec2D(x))
      const poly = new toxi.geom.Polygon2D(polyPoints)
      if (poly.containsPoint(pt)) {
        return region
      }
    }
  }


  function init(ctx, canvas) {
    base = startProcessing(ctx, canvas, initState, updateState, draw);
    document.onmousemove = onMouseMove;
  }

  function initState(startTime) {
  }

  function updateState(td, updateTime) {
  }

  function draw(ctx, updateTime) {
    for (let region of regions) {
      ctx.beginPath();
      ctx.lineWidth = 2;
      ctx.strokeStyle = Colour.toColour(darkGreySolid);
      ctx.lineCap = "round";
      ctx.moveTo(region.verticies[0].x, region.verticies[0].y);
      for (let i = 1; i < region.verticies.length; i++) {
        ctx.lineTo(region.verticies[i].x, region.verticies[i].y);
      }
      ctx.lineTo(region.verticies[0].x, region.verticies[0].y);
      if (hoverRegionName && hoverRegionName == region.name) {
        ctx.fillStyle = "rgba(250, 50, 50, 1)";
        ctx.fill()
      }
      ctx.stroke();
    }
  }

  return { 
    init: init
  };

})();
