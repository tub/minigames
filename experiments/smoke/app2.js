'use strict';

const App = (function() {

  let base;
  let smoke = [];
  let wind;

  const windOpts = {
    baseAngle: 0, //angle jitter?
    baseSpeed: 6,
    gusts: {
      speed: [2, 6], //speed jitter?
      periodInMs: [1000, 6000],
      durationInMs: [3000, 8000],
      max: 3,
    }
  }

  const generalSmokeOpts = {
    generate: {from: 0, to: 2500},
    particlesPerSecond: 45,
    // the amount jittered on the x and y axis per operation
    // Its a bit crap, so we may want to replace it at some point
    jitterPerTick: 0.75,
  }

  //gausian random functions would probably be better
  const smokeParticleOptsFn = () => {
    const angle = 0; //270 degrees, UP
    const spread = 2 * Math.PI; //60 degrees
    const speed = [20, 30];
    const startRadius = [25, 35];
    const endRadius = [50, 60];
    const colourStages = [
      { at: 0, colourRange: {
        from: [darkerGreyPartTransparent, darkerGreyPartTransparent],
        to:   [midGreyPartTransparent, midGreyPartTransparent] }},
      { at: 0.1, colourRange: {
        from: [darkerGrey, darkerGreyPartTransparent],
        to:   [midGrey, midGreyPartTransparent]}},
      { at: 0.6, colourRange: {
        from: [lightGrey, lightGreyPartTransparent],
        to:   [lighterGrey, lighterGreyPartTransparent]}},
      { at: 1, colourRange: {
        from: [lightGreyPartTransparent, lightGreyPartTransparent],
        to:   [lighterGreyPartTransparent, lighterGreyPartTransparent] }}
    ];
    const lifetime = 4000;
    const pos = { x: 300, y: 300 };
    const startDistance = [5, 40];
    return {
      angle,
      spread,
      speed,
      startRadius,
      endRadius,
      colourStages,
      lifetime,
      pos,
      startDistance
    }
  }

  function init(ctxparam, canvasParam, document, canvasSizeParam) {
    base = startProcessing(ctx, canvas, initState, updateState, draw);
  }

  function initState(startTime) {
    wind = new Wind(windOpts);
    smoke.push(new Smoke(startTime, generalSmokeOpts, smokeParticleOptsFn));
  }

  function updateState(td, updateTime) {
    wind.updateState(td, updateTime);
    smoke.forEach(s => s.updateState(td, updateTime, { wind: wind.getWindState() }));
  }

  function draw(updateTime) {
    smoke.forEach(s => s.draw(ctx));
  }

  return { 
    init: init
  };

})();
