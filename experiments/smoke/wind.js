'use strict';

const Wind = function(windOpts) {

  let windState = {
    nextGust: null,
    velocity: null,
    gustSpeed: 0,
    gusts: []
  }

  function getWindState() {
    return windState;
  }

  function updateState(td, updateTime) {
    updateWindState(td, updateTime);
  }

  //PERF: wind state really doesn't need to be updated every frame
  function updateWindState(td, updateTime) {
    let nextState = {}

    //do we need to end a gust?
    let gusts = windState.gusts.filter(g => g.endsAt > updateTime);

    let nextGust = windState.nextGust;
    if (nextGust == null) {
      nextGust = updateTime + Math.random() * (windOpts.gusts.periodInMs[1] - windOpts.gusts.periodInMs[0]) + windOpts.gusts.periodInMs[0];
    }

    //do we need a new guest
    if (nextGust < updateTime) {
      nextGust = null;
      if (gusts.length < windOpts.gusts.max) {
        gusts.push({
          speed: Math.random() * (windOpts.gusts.speed[1] - windOpts.gusts.speed[0]) + windOpts.gusts.speed[0],
          endsAt: updateTime + Math.random() * (windOpts.gusts.durationInMs[1] - windOpts.gusts.durationInMs[0]) + windOpts.gusts.durationInMs[0],
        })
      }
    }

    //update wind velocity
    const previousGustSpeed = windState.gustSpeed;
    let newGustSpeed = windState.gusts.reduce((acc, g) => acc + g.speed, 0);

    //wind speed should only change by up to a certian amount per second
    //I'm not sure if the 18 should be harcoded at this point, but lets leave it for now
    const maxGustChangeOverPeriod = 18 * td/1000;
    if (Math.abs(newGustSpeed - previousGustSpeed) > maxGustChangeOverPeriod) {
      if (newGustSpeed > previousGustSpeed) {
        newGustSpeed = previousGustSpeed + maxGustChangeOverPeriod;
      } else {
        newGustSpeed = previousGustSpeed - maxGustChangeOverPeriod;
      }
    }

    const windAngle = windOpts.baseAngle;
    const speed = windOpts.baseSpeed + newGustSpeed;
    const velocity = toxi.geom.Vec2D.fromTheta(windAngle).scale(speed);

    windState = {
      velocity,
      nextGust,
      gustSpeed: newGustSpeed,
      gusts
    };
  }

  return {
    getWindState,
    updateState
  }

}
