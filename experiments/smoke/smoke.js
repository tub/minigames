'use strict';

const Smoke = function(time, globalOpts, particleOptsFn) {

  let prevParticleTime = time;
  let particles = [];

  function updateState(td, updateTime, global) {
    updateParticles(td, updateTime, global);

    const msPerParticle = 1000/globalOpts.particlesPerSecond;

    while (updateTime - prevParticleTime > msPerParticle) {
      prevParticleTime = prevParticleTime + msPerParticle;
      if (isWithinGeneratingTime(prevParticleTime)) {
        particles.push(createParticle(updateTime, particleOptsFn()));
      }
    }
  }

  function isWithinGeneratingTime(particleTime) {
    return !globalOpts.generate || prevParticleTime >= globalOpts.generate.from && prevParticleTime < globalOpts.generate.to;
  }

  function createParticle(updateTime, opts) {
    const speed = Math.random() * (opts.speed[1] - opts.speed[0]) + opts.speed[0];
    const randWithinSpread = Math.random() * opts.spread;
    const angle = opts.angle + randWithinSpread - opts.spread/2;
    const velocity = toxi.geom.Vec2D.fromTheta(angle).scale(speed);

    const startRadius = Math.random() * (opts.startRadius[1] - opts.startRadius[0]) + opts.startRadius[0];
    const endRadius = Math.random() * (opts.endRadius[1] - opts.endRadius[0]) + opts.endRadius[1];

    const colourRand = Math.random();

    const colour0 = Colour.getMixColour(
      opts.colourStages[0].colourRange.from[0],
      opts.colourStages[0].colourRange.to[0],
      colourRand);
    const colour1 = Colour.getMixColour(
      opts.colourStages[0].colourRange.from[1],
      opts.colourStages[0].colourRange.to[1],
      colourRand);

    let startDistance = opts.startDistance || 0;
    if (Array.isArray(startDistance)) {
      startDistance = Math.random() * (startDistance[1] - startDistance[0]) + startDistance[0];
    }

    const x = opts.pos.x + Math.cos(angle) * startDistance;
    const y = opts.pos.y + Math.sin(angle) * startDistance;

    return {
      x, y,
      velocity,
      radius: startRadius,
      startRadius,
      endRadius,
      colourRand,
      colour0,
      colour1,
      colourStages: opts.colourStages,
      startTime: updateTime,
      lifetime: opts.lifetime
    };
  }

  function updateParticles(td, updateTime, global) {
    const tdInSeconds = td/1000;
    particles = particles.map(p => {
      if (p.startTime + p.lifetime < updateTime) {
        return null;
      }
      //wind doesn't affect particles consistantly
      const windAffect = global.wind.velocity.scale((Math.random() * 1.8 + 0.2));
      const positionChangeOverPeriod = p.velocity.add(windAffect).scale(tdInSeconds)
      const pos = positionChangeOverPeriod.add(p);
      const radius = p.startRadius + (p.endRadius - p.startRadius) * easing.linear((updateTime - p.startTime)/p.lifetime)
      const xjitter = (Math.random() * 2 * globalOpts.jitterPerTick) - globalOpts.jitterPerTick;
      const yjitter = (Math.random() * 2 * globalOpts.jitterPerTick) - globalOpts.jitterPerTick;
      const velocity = p.velocity.add(xjitter, yjitter); //jitter

      const durationRatio  = (updateTime - p.startTime) / p.lifetime;
      const [colour0, colour1] = getStageColour(p.colourStages, durationRatio, p.colourRand);

      return Object.assign({}, p, {x: pos.x, y: pos.y, radius, velocity, colour0, colour1});
    });

    particles = _.compact(particles)
  }

  function getStageColour(colourStages, durationRatio, colourRand) {
    let i = 0;
    while (durationRatio > colourStages[i].at) {
      i++;
    }
    const prevStage = i == 0 ? colourStages[0] : colourStages[i-1];
    const nextStage = colourStages[i];

    const colourA0 = Colour.getMixColour(prevStage.colourRange.from[0], prevStage.colourRange.to[0], colourRand);
    const colourA1 = Colour.getMixColour(prevStage.colourRange.from[1], prevStage.colourRange.to[1], colourRand);

    if (nextStage.at === prevStage.at || durationRatio == nextStage.at) {
      return [colourA0, colourA1]
    }

    const colourB0 = Colour.getMixColour(nextStage.colourRange.from[0], nextStage.colourRange.to[0], colourRand);
    const colourB1 = Colour.getMixColour(nextStage.colourRange.from[1], nextStage.colourRange.to[1], colourRand);

    const transitionPoint = (durationRatio - prevStage.at) / (nextStage.at - prevStage.at);

    const colour0 = Colour.getMixColour(colourA0, colourB0, transitionPoint);
    const colour1 = Colour.getMixColour(colourA1, colourB1, transitionPoint);
    return [colour0, colour1];
  }

  function draw(ctx) {
    drawParticles(ctx);
  }

  function drawParticles() {
    //for each particle, draw each background, then penumbra, then main
    particles.forEach(p => {
      const gradient = ctx.createRadialGradient(p.x, p.y, p.radius/4, p.x, p.y, p.radius)
      gradient.addColorStop(0.4, Colour.toColour(p.colour0))
      gradient.addColorStop(1, Colour.toColour(p.colour1))
      ctx.fillStyle = gradient;
      ctx.beginPath();
      ctx.arc(p.x, p.y, p.radius, 0, Math.PI*2, false); // explosion circle
      ctx.fill();
    });
  }

  return {
    draw,
    updateState
  }

}
