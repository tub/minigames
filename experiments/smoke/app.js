'use strict';

const App = (function() {

  let base;
  let smoke = [];
  let wind;

  const windOpts = {
    baseAngle: 0, //angle jitter?
    baseSpeed: 5,
    gusts: {
      speed: [0, 60], //speed jitter?
      periodInMs: [1000, 6000],
      durationInMs: [3000, 8000],
      max: 3,
    }
  }

  const generalSmokeOpts = {
    particlesPerSecond: 28,
    // the amount jittered on the x and y axis per operation
    // Its a bit crap, so we may want to replace it at some point
    jitterPerTick: 0.75
  }

  //gausian random functions would probably be better
  const smokeParticleOptsFn = () => {
    const angle = 1.5 * Math.PI; //270 degrees, UP
    const spread = Math.PI/6; //60 degrees
    const speed = [70, 80];
    const startRadius = [12, 16];
    const endRadius = [80, 90];
    const colourStages = [
      // each colour is made up of two colours, the inner radius, and outer radius colors
      // From and To is the variable range that can be used for the colour. The algorithm will pick
      // a point somewhere between them, from 0 to 1, and stay there throughout the particles duration,
      // so it's better if the step points are consistent.
      { at: 0, colourRange: {
        from: [lightGreyPartTransparent, lightGreyPartTransparent],
        to:   [lighterGreyPartTransparent, lighterGreyPartTransparent] }},
      { at: 0.3, colourRange: {
        from: [lightGrey, lightGreyPartTransparent],
        to:   [lighterGrey, lighterGreyPartTransparent]}},
      { at: 0.6, colourRange: {
        from: [lightGrey, lightGreyPartTransparent],
        to:   [lighterGrey, lighterGreyPartTransparent]}},
      { at: 1, colourRange: {
        from: [lightGreyPartTransparent, lightGreyPartTransparent],
        to:   [lighterGreyPartTransparent, lighterGreyPartTransparent] }}
    ];
    const lifetime = 6000;
    const pos = { x: 125, y: 500 };
    const startDistance = 10;
    return {
      angle,
      spread,
      speed,
      startRadius,
      endRadius,
      colourStages,
      lifetime,
      pos,
      startDistance
    }
  }

  //gausian random functions would probably be better
  const smokeParticleOptsFn2 = () => {
    const angle = 1.5 * Math.PI; //270 degrees, UP
    const spread = Math.PI/6; //60 degrees
    const speed = [70, 80];
    const startRadius = [12, 16];
    const endRadius = [80, 90];
    const colourStages = [
      { at: 0, colourRange: {
        from: [darkerGreyPartTransparent, darkerGreyPartTransparent],
        to:   [midGreyPartTransparent, midGreyPartTransparent] }},
      { at: 0.2, colourRange: {
        from: [darkerGrey, darkerGreyPartTransparent],
        to:   [midGrey, midGreyPartTransparent]}},
      { at: 0.6, colourRange: {
        from: [lightGrey, lightGreyPartTransparent],
        to:   [lighterGrey, lighterGreyPartTransparent]}},
      { at: 1, colourRange: {
        from: [lightGreyPartTransparent, lightGreyPartTransparent],
        to:   [lighterGreyPartTransparent, lighterGreyPartTransparent] }}
    ];
    const lifetime = 6000;
    const pos = { x: 375, y: 500 };
    const startDistance = 10;
    return {
      angle,
      spread,
      speed,
      startRadius,
      endRadius,
      colourStages,
      lifetime,
      pos,
      startDistance
    }
  }

  function init(ctx, canvas) {
    base = startProcessing(ctx, canvas, initState, updateState, draw);
  }

  function initState(startTime) {
    wind = new Wind(windOpts);
    smoke.push(new Smoke(startTime, generalSmokeOpts, smokeParticleOptsFn));
    smoke.push(new Smoke(startTime, generalSmokeOpts, smokeParticleOptsFn2));
  }

  function updateState(td, updateTime) {
    wind.updateState(td, updateTime);
    smoke.forEach(s => s.updateState(td, updateTime, { wind: wind.getWindState() }));
  }

  function draw(ctx, updateTime) {
    smoke.forEach(s => s.draw(ctx));
  }

  return { 
    init: init
  };

})();
