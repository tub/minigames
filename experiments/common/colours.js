const transparent =    { r: 255, g: 255, b: 255, a: 0};
const white =          { r: 255, g: 255, b: 255, a: 1};
const veryBright =     { r: 255, g: 230, b: 150, a: 1};
const burntOrange =    { r: 255, g: 102, b:   0, a: 1};
const brightYellow =   { r: 255, g: 216, b:   0, a: 1};
const strongRed =      { r: 255, g:  55, b:   0, a: 1};
const darkRed =        { r: 220, g:  20, b:   0, a: 1};
const deepGrey =       { r: 70,  g:  30, b:  20, a: 0};

const darkGrey =       { r:  64, g:  64, b:  64, a: 0.5};

const lightGreySolid=  { r: 192, g: 192, b: 192, a: 1};
const midGreySolid =   { r: 128, g: 128, b: 128, a: 1};
const darkGreySolid =  { r:  64, g:  64, b:  64, a: 1};

const lighterGrey =    { r: 215, g: 215, b: 215, a: 0.5};
const lighterGreyPartTransparent = { r: 222, g: 222, b: 222, a: 0};
const lightGrey =      { r: 192, g: 192, b: 192, a: 0.5};
const lightGreyPartTransparent =   { r: 206, g: 206, b: 206, a: 0};
const midGrey =        { r: 160, g: 160, b: 160, a: 0.5};
const midGreyPartTransparent =     { r: 175, g: 175, b: 175, a: 0};
const darkerGrey =     { r: 128, g: 128, b: 128, a: 0.5};
const darkerGreyPartTransparent =  { r: 142, g: 142, b: 142, a: 0};

const Colour = (function() {

  function toColour(c) {
    return `rgba(${c.r},${c.g},${c.b},${c.a})`;
  }

  //linear transition between colours
  function getMixColour(c1, c2, ratio) {
    return {
      r: Math.round((c2.r - c1.r) * ratio + c1.r),
      g: Math.round((c2.g - c1.g) * ratio + c1.g),
      b: Math.round((c2.b - c1.b) * ratio + c1.b),
      a: (c2.a - c1.a) * ratio + c1.a
    }
  }

  return {
    toColour,
    getMixColour
  }
})()
