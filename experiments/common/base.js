
const startProcessing = function(ctx, canvas, initCallback, updateStateCallback, drawCallback) {

  let fpsCounter = 0;
  let fpsCountStart;
  let currentfps = 0;

  let prevUpdateTime = performance.now();
  let startTime = prevUpdateTime;

  //callback to let other parts of the system init
  initCallback && initCallback(startTime, canvas);

  //start the game loop
  window.requestAnimationFrame(mainloop);

  function mainloop() {
    const updateTime = performance.now();
    const td = updateTime - prevUpdateTime;
    prevUpdateTime = updateTime;

    updateState(td, updateTime);

    ctx.clearRect(0, 0, canvas.width, canvas.height); // clear canvas
    draw(updateTime);

    window.requestAnimationFrame(mainloop);
  }

  function updateState(td, updateTime) {
    updateFps(td, updateTime);
    updateStateCallback && updateStateCallback(td, updateTime);
  }

  function draw(updateTime) {
    drawCallback(ctx, updateTime)
    drawFps();
  }

  function updateFps(td, updateTime) {
    if (!fpsCounter) {
      fpsCountStart = updateTime;
      fpsCounter = 1;
    } else if (fpsCounter === 5) {
      //take an sample every 5 frames
      currentfps = 1/((updateTime - fpsCountStart) / 5000);
      fpsCounter = 0;
    } else {
      fpsCounter++;
    }
  }

  function drawFps() {
    ctx.font = '10pt monospace'
    ctx.fillStyle = 'black';
    ctx.textAlign = 'right';
    ctx.fillText(currentfps.toFixed(0) + ' fps', canvas.width - 10, 15)
  }

}
