'use strict';

const App = (function() {


  //use the buildFullColourTransitions() functions to turn this into a more usefull array of
  // {startTime, startColour, endTime, endColour} objects
  const colourTransitionDeclaration = [
    [  0, white],
    [ 50, veryBright],
    [100, brightYellow],
    [250, burntOrange],
    [300, strongRed],
    [375, darkRed],
    [450, deepGrey],
  ];

  let base;
  let particles = [];

  let startTime = null;
  let colourTransitions;

  //explosion takes about half a second - 600 milliseconds

  function init(ctx, canvas) {
    base = startProcessing(ctx, canvas, initState, updateState, draw);
  }

  function initState(st, canvas) {
    startTime = st;
    colourTransitions = buildFullColourTransitions(colourTransitionDeclaration);
    const initialParticle = {
      x: canvas.width / 2,
      y: canvas.height /2
    };

    particles = _.range(0, 75).map(() => {
      const angle = Math.random() * Math.PI*2;
      const speed = (Math.random() * 400) + 60;
      const velocity = toxi.geom.Vec2D.fromTheta(angle).scale(speed)
      //decelerate velocity by this much per second (random between -2.00 and -2.25)
      const acceleration = (Math.random() * -0.8) + -3; 

      const growthAcceleration = -4; //(Math.random() * -0.8) + -3;
      const radiusLimit = 20;

      const subParticles = [{
        //first sub particle is the main one
        radius: 8,
        colourOffset: 0,
        radiusGrowthSpeed: 1
      }, {
        //second sub particle is the penumbra
        radius: 10, //second is slightly larger
        colourOffset: 75, //second part starts later in the colour cycle
        radiusGrowthSpeed: 1.6
      }, {
        //third sub particle is the background
        radius: 12, //third is larger again
        colourOffset: 150, //and starts last in the colour cylce
        radiusGrowthSpeed: 2.2
      }]

      return Object.assign({}, initialParticle, {
        velocity, acceleration,
        subParticles,
        growthAcceleration,
        radiusLimit});
    })
  }

  function updateState(td, updateTime) {
    const tdInSeconds = td/1000;
    particles = particles.map(p => {
      const positionChangeOverPeriod = p.velocity.scale(tdInSeconds)
      const pos = positionChangeOverPeriod.add(p);
      const velocityChangeOverPeriod = p.acceleration * tdInSeconds;
      const velocity = p.velocity.scale(1 + velocityChangeOverPeriod)

      const subParticles = p.subParticles.map(sp => {
        let radius = sp.radius + sp.radiusGrowthSpeed;
        const growthChangeOverPeriod = p.growthAcceleration * tdInSeconds;
        const radiusGrowthSpeed = sp.radiusGrowthSpeed *(1 + growthChangeOverPeriod);
        return Object.assign({}, sp, {radius, radiusGrowthSpeed});
      });
      return Object.assign({}, p, {x: pos.x, y: pos.y, velocity, subParticles});
    });
  }

  function buildFullColourTransitions(colourTransitionDeclartions) {
    return colourTransitionDeclartions.reduce((state, next) => {
      const newTransition = {
        startTime: next[0],
        startColour: next[1]
      }
      if (state.length) {
        const prev = state[state.length - 1];
        const updatedPrev = Object.assign({}, prev, {
          endTime: next[0],
          endColour: next[1],
          duration: next[0] - prev.startTime
        });
        return state.slice(0, -1).concat([updatedPrev, newTransition]);
      }
      return state.concat(newTransition);
    }, []);
  }

  function colorAtTime(time, colourTransitions) {
    const transition = colourTransitions
      .filter(t => time >= t.startTime && (!t.endTime || time < t.endTime))[0];

    if (transition) {
      if (!transition.endColour) {
        return transition.startColour;
      }
      return Colour.getMixColour(
        transition.startColour,
        transition.endColour,
        (time - transition.startTime)/transition.duration);
    } else {
      return '#000000' //black
    }
  }


  function drawExplosion(ctx, updateTime) {
    //for each particle, draw each background, then penumbra, then main
    [2, 1, 0].forEach(phase => {
      particles.forEach(p => {
        const time = updateTime - startTime + p.subParticles[phase].colourOffset;
        ctx.fillStyle = Colour.toColour(colorAtTime(time*1.2, colourTransitions));
        ctx.beginPath();
        ctx.arc(p.x, p.y, p.subParticles[phase].radius, 0, Math.PI*2, false); // explosion circle
        ctx.fill();
      });
    });
  }

  function draw(ctx, updateTime) {
    drawExplosion(ctx, updateTime);
  }

  return { 
    init: init
  };

})();
